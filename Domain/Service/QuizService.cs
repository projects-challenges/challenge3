﻿using AutoMapper;
using Entity;
using Entity.UnitofWork;
using Shared.ViewModels;

namespace Domain.Service
{
    public class QuizService<Tv, Te> : GenericService<Tv, Te>
                                                where Tv : QuizViewModel
                                                where Te : Quiz
    {
        //DI must be implemented in specific service as well beside GenericService constructor
        public QuizService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            if (_unitOfWork == null)
                _unitOfWork = unitOfWork;
            if (_mapper == null)
                _mapper = mapper;
        }

        //add here any custom service method or override generic service method
        public bool DoNothing()
        {
            return true;
        }

    }

}
