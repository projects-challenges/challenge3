﻿using AutoMapper;
using Entity;
using Shared.ViewModels;

namespace Domain.Mapping
{
    public partial class MappingProfile : Profile
    {
        /// <summary>
        /// Create automap mapping profiles
        /// </summary>
        public MappingProfile()
        {
            CreateMap<QuizViewModel, Quiz>().ReverseMap();
            CreateMap<QuestionViewModel, Question>().ReverseMap();

            //call code in partial scaffolded function
            SetAddedMappingProfile();
        }

        //to call scaffolded method
        partial void SetAddedMappingProfile();

    }





}
