﻿using System.ComponentModel.DataAnnotations;

namespace Entity
{
    public class Question : BaseEntity
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
    }
}
