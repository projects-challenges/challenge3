﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entity
{
    public class Quiz : BaseEntity
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(20)]
        public string User { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
    }
}
