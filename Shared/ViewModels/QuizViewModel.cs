﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shared.ViewModels
{
    public class QuizViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(20)]
        public string User { get; set; }
        public virtual ICollection<QuestionViewModel> Questions { get; set; }
    }
}
