﻿using System.ComponentModel.DataAnnotations;

namespace Shared.ViewModels
{
    public class QuestionViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
    }
}
