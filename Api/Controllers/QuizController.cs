﻿using Domain.Service;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Shared.ViewModels;
using System;

namespace Api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class QuizController : BaseController
    {
        private readonly QuizService<QuizViewModel, Quiz> _quizService;
        public QuizController(QuizService<QuizViewModel, Quiz> quizService)
        {
            _quizService = quizService;
        }

        //get all
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var items = _quizService.GetAll();
            return Response(items);
        }

        //get one
        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            var item = _quizService.GetOne(id);
            if (item == null)
            {
                Log.Error("GetById({ ID}) NOT FOUND", id);
                return NotFound();
            }

            return Response(item);
        }

        //add
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Create([FromBody] QuizViewModel quiz)
        {
            if (quiz == null)
                return BadRequest();

            var id = _quizService.Add(quiz);
            return Created($"api/Quiz/{id}", id);  //HTTP201 Resource created
        }

        //update
        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(Guid id, [FromBody] QuizViewModel quiz)
        {
            if (quiz == null || quiz.Id != id)
                return BadRequest();

            int retVal = _quizService.Update(quiz);
            if (retVal == 0)
                return StatusCode(304);  //Not Modified
            else if (retVal == -1)
                return StatusCode(412, "DbUpdateConcurrencyException");  //412 Precondition Failed  - concurrency
            else
                return Accepted(quiz);
        }

        //delete
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            int retVal = _quizService.Remove(id);
            if (retVal == 0)
                return NotFound();  //Not Found 404
            else if (retVal == -1)
                return StatusCode(412, "DbUpdateConcurrencyException");  //Precondition Failed  - concurrency
            else
                return NoContent();   	     //No Content 204
        }

    }
}